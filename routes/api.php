<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HospitalApiController;
use App\Http\Controllers\PharmacyApiController;
use App\Http\Controllers\MinistryApiController;
use App\Http\Controllers\BrandApiController;
use App\Http\Controllers\AppointmentApiController;
use App\Http\Controllers\ProductApiController;
use App\Http\Controllers\UserApiController;
use App\Http\Controllers\VaccineApiController;
use App\Http\Controllers\Medical_centerApiController;
use App\Http\Controllers\CategoryApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
