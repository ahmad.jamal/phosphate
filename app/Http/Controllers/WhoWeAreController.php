<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WhoWeAre;

class WhoWeAreController extends Controller
{


    public function edit()
    {
        $items = WhoWeAre::where('id', 1)->first();
        return view('admin.whoweare.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = WhoWeAre::where('id', 1)->first();
        if ($request->content) {
            $items->content = $request->content;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/phosphate/images'));
        }
       
        $items->save();
        return redirect()->route('whoweare.index');
    }
}
