<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function logout()
    {
        if (Auth::user()->hasRole('Admin')) {
            $role = 'Admin';
        } else if (Auth::user()->hasRole('User')) {
            $role = 'User';
        } else if (Auth::user()->hasRole('Vendor')) {
            $role = 'Vendor';
        }
        Auth::logout();
        if ($role == 'Admin') {
            return redirect('/login');
        } else if ($role == 'User') {
            return redirect('/login');
        } else if ($role == 'Vendor') {
            return redirect('/login');
        }
        return redirect('/login');
    }
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        // dd($credentials);
        if (Auth::attempt($credentials)) {
            if (Auth::user()->pand == 1) {
                Auth::logout();
                return redirect()->route('login');
            }
            // return Auth::user()->hasRole('Admin');
            if (Auth::user()->hasRole('Admin')) {
                return redirect('/user');
            } else if (Auth::user()->hasRole('User')) {
                return redirect('/user');
            } else if (Auth::user()->hasRole('Vendor')) {
                return redirect('/user');
            }
        }
        return redirect()->route('login')->with(['success' => 'كلمة المرور او البريد الالكتروني غير صحيح']);
    }

    public function login2(Request $request)
    {
        $credentials = request(['email', 'password']);
        // dd($credentials);
        $validator = Validator::make($request->all(), [
            'email' => 'required',   // required and email format validation
            'password' => 'required|min:6', // required and number field validation

        ]); // create the validations

        if ($validator->fails())   //check all validations are fine, if not then redirect and show error messages
        {
            return response()->json($validator->errors(), 422);
            // validation failed return with 422 status
        } else {
            if (Auth::attempt($credentials)) {
                if (Auth::user()->pand == 1) {
                    Auth::logout();
                    return response()->json(["status" => true, "msg" => "pand", "redirect_location" => route("login")]);
                }
                // return Auth::user()->hasRole('Admin');
                if (Auth::user()->hasRole('Admin')) {
                    return response()->json(["status" => true, "msg" => "You have successfully Logedin", "redirect_location" => route("dashboard-ecommerce")]);
                } else if (Auth::user()->hasRole('User')) {
                    return response()->json(["status" => true, "msg" => "You have successfully Logedin", "redirect_location" => route("front.home")]);
                } else if (Auth::user()->hasRole('Vendor')) {
                    return response()->json(["status" => true, "msg" => "You have successfully Logedin", "redirect_location" => route("dashboard-ecommerce")]);
                }
            } else {
                return response()->json('the email is not exist or the password not correct', 422);
            }
        }
        return redirect()->route('front.home')->with(['success' => 'كلمة المرور او البريد الالكتروني غير صحيح']);
    }
}
