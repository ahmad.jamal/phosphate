<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContactUs;

class ContactusController extends Controller
{


    public function edit()
    {
        $items = ContactUs::where('id', 1)->first();
        return view('admin.contactus.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = ContactUs::where('id', 1)->first();
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->phone) {
            $items->phone = $request->phone;
        }
        if ($request->email) {
            $items->email = $request->email;
        }

        $items->save();
        return redirect()->route('contactus.index');
    }
}
