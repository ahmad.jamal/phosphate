<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $items = News::all();
        return view('admin.news.index', compact('items'));
    }

    public function create()
    {
        return view('admin.news.create');
    }

    public function store(Request $request)
    {
        $items = new News();
        $items->f_title = $request->f_title; 
        $items->s_title = $request->s_title;      
        $items->content = $request->content; 
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/news/images'));
        }                                                     
        $items->save();
        
        return redirect()->route('news.index');
    }

    public function edit($id)
    {
        $items = News::find($id);
        return view('admin.news.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = News::find($id);
        if ($request->f_title) {
            $items->f_title = $request->f_title;
        }
        if ($request->s_title) {
            $items->s_title = $request->s_title;
        }
        if ($request->content) {
            $items->content = $request->content;
        }
        $items->save();
        return redirect()->route('news.index');
    }


    public function destroy($id)
    {
        $items = News::find($id);
        $items->delete();
        return redirect()->route('news.index');
    }
}
