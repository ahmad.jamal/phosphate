<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{
    //
    public function swap($locale)
    {
        
        //z available language in template array
        $availLocale = ['en' => 'en', 'ar' => 'ar', 'de' => 'de', 'pt' => 'pt'];
        // check for existing language
        if (array_key_exists($locale, $availLocale)) {
            session()->put('locale', $locale);
            App::setLocale($locale);
            if ($locale == 'ar') {
                config(['MIX_CONTENT_DIRECTION' => 'rtl']);
                session()->put('direction', 'rtl');
            } else {
                config(['MIX_CONTENT_DIRECTION' => 'ltr']);
                session()->put('direction', 'ltr');
            }
            // dd(session()->get('locale'));
        }
        // dd(session()->get('locale'));
        return redirect()->back();
    }
}
