<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mission;

class MissionController extends Controller
{
    public function edit()
    {
        $items = Mission::where('id', 1)->first();
        return view('admin.mission.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Missions::where('id', 1)->first();
        if ($request->content) {
            $items->content = $request->content;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/mission/images'));
        }
       
        $items->save();
        return redirect()->route('mission.index');
    }
}
