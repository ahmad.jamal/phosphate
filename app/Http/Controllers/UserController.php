<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $items = User::all();
        return view('admin.user.index', compact('items'));
    }
    public function getNotCount(Request $request)
    {
        return count(Notification::where('user_sent_to_id', auth()->user()->id)->get());
    }

    public function hasToken()
    {
        if (auth()->check()) {
            $user = User::find(auth()->user()->id);
            if ($user->fcmToken != null) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 0;
        }
    }
    public function storeToken(Request $request)
    {
        if (auth()->check()) {
            $user = User::find(auth()->user()->id);
            $user->fcmToken = $request->token;
            $user->save();
            return 1;
        } else {
            return 0;
        }
    }
    public function create()
    {
        $roles = Role::all();
        return view('admin.user.create', compact('roles'));
    }
    public function store(Request $request)
    {
        // dd($request);
        // return $request;
        $request->validate([
            'email' => 'email|unique:users',
        ]);
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->password = Hash::make($request->password);
        $items->address =  $request->address;
        $items->save();
        
        $items->assignRole('User');
        return redirect()->route('user.index');
    }
    public function edit($id)
    {
        $items = User::find($id);
        // dd($items);
        $roles = Role::all();
        return view('admin.user.edit', compact('items', 'roles'));
    }
    public function update(Request $request, $id)
    {


        $items = User::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->role_name) {
            $items->assignRole($request->role_name);
        }
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/user/images'));
        }
        $items->save();
        return redirect()->route('user.index');
    }
    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        return redirect()->route('user.index');
    }

    public function ShowProfile($id)
    {
        $items = User::all($id);
        return view('admin.user.profile', compact('items', 'roles'));
    }
}
