<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Responsibilities;

class ResponsibilitiesController extends Controller
{
    public function index()
    {
        $items = Responsibilities::all();
        return view('admin.responsibilities.index', compact('items'));
    }

    public function create()
    {
        return view('admin.responsibilities.create');
    }

    public function store(Request $request)
    {
        $items = new Responsibilities();   
        $items->content = $request->content;                                                     
        $items->save();
        
        return redirect()->route('responsibilities.index');
    }

    public function edit($id)
    {
        $items = Responsibilities::find($id);
        return view('admin.responsibilities.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Responsibilities::find($id);
        if ($request->content) {
            $items->content = $request->content;
        }
        $items->save();
        return redirect()->route('responsibilities.index');
    }


    public function destroy($id)
    {
        $items = Responsibilities::find($id);
        $items->delete();
        return redirect()->route('responsibilities.index');
    }
}
