<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WhereWeAre;

class WhereWeAreController extends Controller
{
    public function index()
    {
        $items = WhereWeAre::all();
        return view('admin.whereweare.index', compact('items'));
    }

    public function create()
    {
        return view('admin.whereweare.create');
    }

    public function store(Request $request)
    {
        $items = new WhereWeAre();
        $items->address = $request->address; 
        $items->f_phone = $request->f_phone;
        $items->s_phone = $request->s_phone;       
        $items->email = $request->email;                                                      
        $items->save();
        
        return redirect()->route('whereweare.index');
    }

    public function edit($id)
    {
        $items = WhereWeAre::find($id);
        return view('admin.whereweare.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = WhereWeAre::find($id);
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->f_phone) {
            $items->f_phone = $request->f_phone;
        }
        if ($request->s_phone) {
            $items->s_phone = $request->s_phone;
        }
        if ($request->email) {
            $items->email = $request->email;
        }
        $items->save();
        return redirect()->route('whereweare.index');
    }


    public function destroy($id)
    {
        $items = WhereWeAre::find($id);
        $items->delete();
        return redirect()->route('whereweare.index');
    }
}
