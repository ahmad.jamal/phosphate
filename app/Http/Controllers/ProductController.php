<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Spatie\Permission\Models\Role;

class ProductController extends Controller
{
    public function index()
    {
        $items = Product::all();
        return view('admin.product.index', compact('items'));
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function store(Request $request)
    {
        $items = new Product();
        $items->name = $request->name;      
        $items->description = $request->description;                                                     
        $items->save();
        
        return redirect()->route('product.index');
    }

    public function edit($id)
    {
        $items = Product::find($id);
        return view('admin.product.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Product::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->description) {
            $items->description = $request->description;
        }
        $items->save();
        return redirect()->route('product.index');
    }


    public function destroy($id)
    {
        $items = Product::find($id);
        $items->delete();
        return redirect()->route('product.index');
    }
}
