<?php

namespace App\Console\Commands;

use App\Models\Offer;
use Illuminate\Console\Command;
use Carbon\Carbon;

class editOffer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edit:Offer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User Can Edit The Offer Withn 24 h';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $Offers = Offer::all();
        foreach ($Offers as $key => $value) {
            $date2 = Carbon::parse($value->created_at);
            $date1 = Carbon::now();
            if ($date2->addHour(24) < $date1) {
                // return $value;
                $value->canEdit = 0;
                $value->save();
            }
        }
        return Command::SUCCESS;
    }
}
