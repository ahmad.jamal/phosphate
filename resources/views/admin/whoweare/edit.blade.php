@extends('layouts/contentLayoutMaster')

@section('title', 'Edit')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Vertical Wizard -->
        <div class="card-body">
            <form action="{{ route('who.update', $items->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-1 row col-12">
                    <div class="col-6">
                        <label class="form-label" for="basic-addon-name">Content</label>
                        <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Content"
                            name="content"
                            value="{{$items->content}}"
                            aria-label="Content"
                            aria-describedby="basic-addon-name"
                            required
                        />
                    </div>
                   
                </div>
                <div class="mb-1 row col-12">
                    <!-- <div class="col-6">
                        <label class="form-label" for="basic-addon-name">Description</label>
                        <textarea
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Description"
                            name="description"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            required
                        >{{$items->description}}</textarea>
                    </div> -->
                    
                </div>
               
     

                <div class="row col-12">
                   
                  
                   
                </div>


                </div>

                <div class="row col-12">
                    <label style="margin-left: 22px;" class="form-label " for="vertical-username">Image</label>
                    <div class=" col-6">
                        <!-- remove thumbnail file upload starts -->

                        <div class="card-body">
                            <input class="form-control" id="uploadImage5" type="file" name="image"/>
                                                <img style="width: 129px; margin-top: 2%;" id="uploadPreview5" src="{{asset($items->image)}}" alt="your image" />
                        </div>
                    </div>
                    {{-- <div class="row col-6">
                        <!-- remove thumbnail file upload starts -->
                        <label class="form-label" for="vertical-username">Image From Gallery</label>
                        <div class="card-body">
                            <input class="form-control" type="file" name="image2"/>
                        </div>
                    </div> --}}
                </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
  <!-- /Vertical Wizard -->
      </div>
    </div>
  </div>
</div>
@endsection
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>

@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $( document ).ready(function() {
 // console.log( $("#uploadImage").attr);
 $("#uploadImage5").change(function () {

if (this.files && this.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#uploadPreview5').attr('src', e.target.result);
    }
    reader.readAsDataURL(this.files[0]);
}
});
});
</script>
