@extends('layouts/contentLayoutMaster')

@section('title', __('Contact Information'))

@section('vendor-style')
<!-- vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<!-- vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="alert alert-primary" role="alert">
            <div class="alert-body">
                <!-- Vertical Wizard -->
                <div class="card-body">
                    <form action="{{ route('contactinfo.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-1">

                            <div class="row col-12 mb-2">
                                <div class="col-6">
                                    <label class="form-label" for="basic-addon-name">{{ __('Phone') }}</label>
                                    @if(!empty($items->phone))
                                    <input type="text" id="basic-addon-name" class="form-control" name="phone" value="{{$items->phone}}" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @else
                                    <input type="text" id="basic-addon-name" class="form-control" name="phone" value="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @endif
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="basic-addon-name">{{ __('Email') }}</label>
                                    @if(!empty($items->email))
                                    <input type="text" id="basic-addon-name" class="form-control" value="{{$items->email}}" name="email" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @else
                                    <input type="text" id="basic-addon-name" class="form-control" value="" name="email" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @endif
                                </div>
                            </div>
                            <div class="row col-12 mb-2">
                                <div class="col-6">
                                    <label class="form-label" for="basic-addon-name">{{ __('Instagram') }}</label>
                                    @if(!empty($items->instagram))
                                    <input type="text" id="basic-addon-name" class="form-control" name="instagram" value="{{$items->instagram}}" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @else
                                    <input type="text" id="basic-addon-name" class="form-control" name="instagram" value="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @endif
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="basic-addon-name">{{ __('Facebook') }}</label>
                                    @if(!empty($items->facebook))
                                    <input type="text" id="basic-addon-name" class="form-control" value="{{$items->facebook}}" name="facebook" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @else
                                    <input type="text" id="basic-addon-name" class="form-control" value="" name="facebook" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @endif
                                </div>
                            </div>
                            <div class="row col-12 mb-2">
                                <div class="col-6">
                                    <label class="form-label" for="basic-addon-name">{{ __('Twitter') }}</label>
                                    @if(!empty($items->twitter))
                                    <input type="text" id="basic-addon-name" class="form-control" value="{{$items->twitter}}" name="twitter" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @else
                                    <input type="text" id="basic-addon-name" class="form-control" value="" name="twitter" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @endif
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="basic-addon-name">{{ __('Youtube') }}</label>
                                    @if(!empty($items->youtube))
                                    <input type="text" id="basic-addon-name" class="form-control" value="{{$items->youtube}}" name="youtube" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @else
                                    <input type="text" id="basic-addon-name" class="form-control" value="" name="youtube" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @endif
                                </div>
                            </div>


                            <div class="row col-12 mb-2">
                                <div class="col-12">
                                    <label class="form-label" for="basic-addon-name">{{ __('Address') }}</label>
                                    @if(!empty($items->address))
                                    <input type="text" id="basic-addon-name" class="form-control" placeholder="Address" name="address" value="{{$items->address}}" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @else
                                    <input type="text" id="basic-addon-name" class="form-control" placeholder="Address" name="address" value="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    @endif
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </form>
                </div>
                <!-- /Vertical Wizard -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

@endsection
