@extends('layouts/contentLayoutMaster')

@section('title', 'Product Images')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Responsive Datatable -->
        <section id="responsive-datatable">
            <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="card-header border-bottom">
                    <h4 class="card-title">Product Images</h4>
                </div>
                <div class="card-datatable">
                    <table class="product table">
                        <thead>
                            <tr>
                            <th></th>
                            <th>Link</th>
                            <th >{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <body>
                            @foreach ($ProductImage as $item)
                                <tr>
                                    <td></td>
                                    <td>
                                        <img style="width: auto; height: auto; max-width: 200px;" src="{{asset($item->link)}}">
                                    </td>
                                    <td >
                                        <div class="row col-12">
                                            
                                            <div class="col-6">
                                                <form method="post"
                                                action="{{route('product.deleteImage',$item-> id)}}">
                                                @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger"  onclick="return confirmBlock()"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </body>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </section>
        <!--/ Responsive Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>
@endsection
