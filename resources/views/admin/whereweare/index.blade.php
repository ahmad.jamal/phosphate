@extends('layouts/contentLayoutMaster')

@section('title', 'Where We Are')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Responsive Datatable -->

        <section id="responsive-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header border-bottom">
                            <h4 class="card-title">Where We Are </h4>
                            <a href="{{route('where.create')}}"><button style="    background-color: #c1f36a !important;
                                border-color: #c1f36a !important;" type="button" class="btn btn-primary">Add Information</button></a>
                        </div>
                        <div class="card-datatable" style="    overflow: auto;">
                            <table class="brand compact table" id="defulte">
                                <thead>
                                  <tr>
                                    <th></th>
                                    
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>F-Phone</th>
                                    <th>S-Phone</th>
                                    {{-- <th>Arabic Description</th>
                                    <th>English Description</th> --}}
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                  <body>
                                      @foreach ($items as $item)
                                      <tr>
                                          <td></td>
                                          <td>{{$item->address}}</td>
                                          <td>{{$item->email}}</td>
                                          <td>{{$item->f_phone}}</td>
                                          <td>{{$item->s_phone}}</td>
                                          {{-- <td>{{$item->descriptionAR}}</td>
                                          <td>{{$item->descriptionEN}}</td> --}}
                                          <td>
                                              <img style="width: auto; height: auto; max-width: 200px;" src="{{asset($item->image)}}">
                                          </td>
                                          
                                          
                                          
                                         
                                
                                          <td>
                                <div class="row col-12">
                                
                                    <div class="col-4">
                                        <a href="{{route('where.edit',$item-> id)}}"
                                            class="btn btn-success">
                                            <i class="icon-pencil"></i> {{__('Edit')}}
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <form method="post"
                                        action="{{route('where.delete',$item-> id)}}">
                                        @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"  onclick="return confirmBlock()"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                                      </tr>
                                      @endforeach
                                  </body>

                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Responsive Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>
@endsection
