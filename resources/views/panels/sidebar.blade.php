@php
$configData = Helper::applClasses();
@endphp
<div class="main-menu menu-fixed {{(($configData['theme'] === 'dark') || ($configData['theme'] === 'semi-dark')) ? 'menu-dark' : 'menu-light'}} menu-accordion menu-shadow"
    data-scroll-to-active="true">
    <div id="page" class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto">
                <a class="navbar-brand" href="{{url('/')}}">
                    <span class="brand-logo">
                        <!-- <img  id="uploadPreview"
                            src="{{asset('front/imgs/logo.png')}}" alt="your image" /> -->
                    </span>
                    <h2 class="brand-text"><span style="color: #ff00008a"> Phosphate </span> </h2>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pe-0" data-toggle="collapse">
                    <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i>
                    <i class="d-none d-xl-block collapse-toggle-icon font-medium-4 text-primary" data-feather="disc"
                        data-ticon="disc"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            {{-- Foreach menu item starts --}}

            @if(isset($menuData[0]))

            @foreach($menuData[0]->menu as $menu)

            @if(isset($menu->navheader))
            <li class="navigation-header">
                <span>{{ __($menu->navheader) }}</span>
                <i data-feather="more-horizontal"></i>
            </li>
            @else
            @if ($menu->slug != '')
            @if (auth()->user()->hasPermissionTo($menu->slug))
            {{-- Add Custom Class with nav-item --}}
            @php
            $custom_classes = "";
            if(isset($menu->classlist)) {
            $custom_classes = $menu->classlist;
            }
            @endphp
            <li class="nav-item {{ $custom_classes }}  {{url()->current() == url($menu->url) ? 'active' : ''}}">
                <a href="{{isset($menu->url)? url($menu->url):'javascript:void(0)'}}" class="d-flex align-items-center"
                    target="{{isset($menu->newTab) ? '_blank':'_self'}}">
                    <i data-feather="{{ $menu->icon }}"></i>
                    @if (session()->get('locale') == 'ar')
                    @if ($menu->name == "Categories")
                    <span class="menu-title text-truncate"> الفئات  </span>
                    @endif
                    @if ($menu->name == "Product Options")
                    <span class="menu-title text-truncate"> خيارات المنتج </span>
                    @endif
                    @if ($menu->name == "Products")
                    <span class="menu-title text-truncate"> المنتجات </span>
                    @endif
                    @if ($menu->name == "Clients")
                    <span class="menu-title text-truncate"> العملاء </span>
                    @endif
                    @if ($menu->name == "Orders")
                    <span class="menu-title text-truncate"> الطلبات </span>
                    @endif
                    @if ($menu->name == "Setting")
                    <span class="menu-title text-truncate"> الاعدادات </span>
                    @endif
                    @if ($menu->name == "Contact Info")
                    <span class="menu-title text-truncate"> معلومات التواصل </span>
                    @endif
                    @if ($menu->name == "Dashboards")
                    <span class="menu-title text-truncate"> لوحة التحكم </span>
                    @endif
                    @if ($menu->name == "Brands")
                    <span class="menu-title text-truncate">العلامات التجارية</span>
                    @endif
                    @if ($menu->name == "Rents")
                    <span class="menu-title text-truncate"> الأجارات</span>
                    @endif
                    @if ($menu->name == "Adds")
                    <span class="menu-title text-truncate"> الاعلانات</span>
                    @endif
                    @if ($menu->name == "Questions")
                    <span class="menu-title text-truncate"> الاسالة الشائعة</span>
                    @endif
                    @if ($menu->name == "Colors")
                    <span class="menu-title text-truncate"> الألوان</span>
                    @endif
                    @if ($menu->name == "Pages")
                    <span class="menu-title text-truncate"> الصفحات</span>
                    @endif
                    @if ($menu->name == "Payemts")
                    <span class="menu-title text-truncate"> الدفعات</span>
                    @endif
                    @if ($menu->name == "Messages")
                    <span class="menu-title text-truncate"> الرسائل</span>
                    @endif
                    @if ($menu->name == "Roles")
                    <span class="menu-title text-truncate"> الأدوار</span>
                    @endif
                    @if ($menu->name == "Permissions")
                    <span class="menu-title text-truncate"> الصلاحيات</span>
                    @endif
                    @if ($menu->name == "Levels")
                    <span class="menu-title text-truncate"> المستويات</span>
                    @endif
                    @if ($menu->name == "Courses")
                    <span class="menu-title text-truncate"> المواد</span>
                    @endif
                    @else
                    <span class="menu-title text-truncate">{{ __($menu->name) }} </span>
                    @endif

                    @if (isset($menu->badge))
                    <?php $badgeClasses = "badge rounded-pill badge-light-primary ms-auto me-1" ?>
                    <span
                        class="{{ isset($menu->badgeClass) ? $menu->badgeClass : $badgeClasses }}">{{$menu->badge}}</span>
                    @endif
                </a>
                @if(isset($menu->submenu))
                @if (session()->get('locale') == 'ar')
                @if ($menu->name == "Analytics")
                @include('panels/submenu', ['menu' => 'الاحصائيات'])
                @endif
                @else
                @include('panels/submenu', ['menu' => $menu->submenu])
                @endif
                @endif
            </li>
            @endif
            @endif
            @endif
            @endforeach
            @endif
            {{-- Foreach menu item ends --}}
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
