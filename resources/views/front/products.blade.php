
    <!---->
    <div id="product-page">
   
      <div class="container">
     
        <div class="row">
          <div class=" col-sm-12"> 
          <div class="justify-content-center div-a">
            <a href="" class="btn-homely-primary">
              Home
            </a>
            <span class="tag-page"><i class="fa fa-arrow-right"></i></span>
            <a class="green-color" href="">
              Products
            </a>
          </div>
          </div>
        </div>
   
    </div>

    
    <!--descriptionproduct-->

      <section id="category" class="container  vtabs1">
        <div class="row">
          <div class="col-md-3 col-sm-12">
            <div class="d-flex align-items-start">
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="cat1" data-toggle="tab" href="#cat1" role="tab" aria-controls="home" aria-selected="true">Fertilizers<img src="images/leaf.png" alt=""></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="cat2" data-toggle="tab" href="#cat2" role="tab" aria-controls="profile" aria-selected="false">Feed phosphates<img src="images/leaf.png" alt=""></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="cat3" data-toggle="tab" href="#cat3" role="tab" aria-controls="contact" aria-selected="false">Phosphate Rock<img src="images/leaf.png" alt=""></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="cat14" data-toggle="tab" href="#cat14" role="tab" aria-controls="contact" aria-selected="false">Industrial phosphates<img src="images/leaf.png" alt=""></a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-9 col-12 right-sec">
            <div class="right-title"><p class="title-cat">Fertilizers</p>
            <p class="details-cat">Nitrogen-phosphorus and complex fertilizers (APAVIVA®)
              Nitrogen-phosphorus and complex fertilizers with microelements (APAVIVA+®)</p></div>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="cat1" role="tabpanel" aria-labelledby="cat1">
                <div class="row">
                  <div class="col-md-6 col-sm-12"> 
                    <div class="row">
                      <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                    <div class="col-md-6 col-sm-12"> 
                      <div class="product-details">
                        <h2>Urea</h2>
                        <p class="sub-title-product">N 46.2   </p>
                        <p>The most concentrated granular nitrogen fertilizer  </p>
                        
                      </div>
                    </div>
               
                   
                </div>
                  </div>
                  <div class="col-md-6 col-sm-12"> 
                    <div class="row">
                      <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                    <div class="col-md-6 col-sm-12"> 
                      <div class="product-details">
                        <h2>Urea</h2>
                        <p class="sub-title-product">N 46.2   </p>
                        <p>The most concentrated granular nitrogen fertilizer  </p>
                        
                      </div>
                    </div>
               
                    
                   </div>
                 </div>
                 <div class="col-md-6 col-sm-12"> 
                  <div class="row">
                    <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                  <div class="col-md-6 col-sm-12"> 
                    <div class="product-details">
                      <h2>Urea</h2>
                      <p class="sub-title-product">N 46.2   </p>
                      <p>The most concentrated granular nitrogen fertilizer  </p>
                      
                    </div>
                  </div>
             
                 
              </div>
                </div>
                <div class="col-md-6 col-sm-12"> 
                  <div class="row">
                    <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                  <div class="col-md-6 col-sm-12"> 
                    <div class="product-details">
                      <h2>Urea</h2>
                      <p class="sub-title-product">N 46.2   </p>
                      <p>The most concentrated granular nitrogen fertilizer  </p>
                      
                    </div>
                  </div>
             
                  
                 </div>
               </div>
               <div class="col-md-6 col-sm-12"> 
                <div class="row">
                  <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                <div class="col-md-6 col-sm-12"> 
                  <div class="product-details">
                    <h2>Urea</h2>
                    <p class="sub-title-product">N 46.2   </p>
                    <p>The most concentrated granular nitrogen fertilizer  </p>
                    
                  </div>
                </div>
           
               
            </div>
              </div>
              <div class="col-md-6 col-sm-12"> 
                <div class="row">
                  <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                <div class="col-md-6 col-sm-12"> 
                  <div class="product-details">
                    <h2>Urea</h2>
                    <p class="sub-title-product">N 46.2   </p>
                    <p>The most concentrated granular nitrogen fertilizer  </p>
                    
                  </div>
                </div>
           
                
               </div>
             </div>
               </div>
              </div>
              <div class="tab-pane fade" id="cat2" role="tabpanel" aria-labelledby="cat2">...</div>
              <div class="tab-pane fade" id="cat3" role="tabpanel" aria-labelledby="cat3">...</div>
              <div class="tab-pane fade" id="cat3" role="tabpanel" aria-labelledby="cat4">...</div>
            </div>
          </div>
        </div>
      </section>
  

