
    <!--banner-section-->
    <div id="banner-section-2">
      
        <div id="" class="" data-ride="carousel">
   
          
          <div class="">
            <div class="">
              <img class="d-block w-100" src="images/Underground-1024x668.png" alt="First slide">
              <div class="text-slider">
              <a class="btn-menu" data-toggle="modal" data-target="#menuModal">

              </a>

              <div class="main-header">

              <div class="text-left">
              <h1 >Our Mission

              </h1>
              <h3>
                We help the world grow the food it needs

              </h3>

              <div class="justify-content-center div-a">
                <a href="" class="btn-homely-primary">
                  Home
                </a>
                <span class="tag-page"><i class="fa fa-arrow-right"></i></span>
                <a class="green-color" href="" class="btn-homely-primary">
                  About Us
                </a>
              </div>
              </div>
              </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <!--Our Responsible-->
    <div id="OurResponsible">
      <div class="header-title text-center">
      <h2 class="">Our Responsible</h2>
      <p class="header-text">WE ARE RESPONSIBLE, INNOVATIVE, COLLABORATIVE AND DRIVEN</p>
     </div>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            
            <div class="news-text">
            <img src="images/safety.png" alt="">
            <p>We are accountable for the safety and wellbeing of our colleagues and our company.</p>
            
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
           
            <div class="news-text">
              <img src="images/quality.png" alt="">
            <p>We foster innovation and encourage ideas that make us better.</p>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
          
            <div class="news-text">
              <img src="images/idea.png" alt="">
            <p>We foster innovation and encourage ideas that make us better.</p>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            
            <div class="news-text">
            <img src="images/balance-scale.png" alt="">
            <p>We act with integrity and conviction.</p>
            
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
           
            <div class="news-text">
              <img src="images/compass.png" alt="">
            <p>We collaborate across departments and geographies to accelerate our performance.</p>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
          
            <div class="news-text">
              <img src="images/drops.png" alt="">
            <p>We are careful stewards of natural resources.</p>
              
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--about-us-->
    <div id="about-us">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12"> <img src="images/split-img1.png" alt=""></div>
          <div class="col-md-6 col-sm-12">
            <div class="left-sec-text">
              <h3>?Who We Are</h3>
              <P>Phosphate rock is processed to produce phosphorous, which is one of the three main nutrients most commonly used in fertilizers (the other two are nitrogen and potassium). Phosphate can also be turned into phosphoric acid, which is used in everything from food and cosmetics to animal feed and electronics. At OCP we adapt our phosphate resources to deliver customized fertilizers for the </P>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!--video-->
    <div id="location">
      <div class="header-title text-center">
        <h2 class="">WHere We Are</h2>
        <p class="header-text">SERVING THE MOST PROMISING AGRICULTURAL REGIONS</p>
       </div>
       <div class="container">
         <div class="row">
          <div class="col-md-4 col-sm-12">
            <div class="location-text"><i class="fa fa-map-marker"></i><p>119333, Bld 1, 55/1, Leninsky Prospekt, Moscow, Russian Federation</p></div>
            <div class="location-text"><i class="fa fa-phone"></i><p>7(099)2364-232-23+</p></div>
            <div class="location-text"><i class="fa fa-phone"></i><p>7(099)2364-232-23+</p></div>
            <div class="location-text"><i class="fa fa-envelope"></i><p>example@hotmail.cpm</p></div>
          </div>
          <div class="col-md-4 col-sm-12 center-location">
            <div class="location-text"><i class="fa fa-map-marker"></i><p>119333, Bld 1, 55/1, Leninsky Prospekt, Moscow, Russian Federation</p></div>
            <div class="location-text"><i class="fa fa-phone"></i><p>7(099)2364-232-23+</p></div>
            <div class="location-text"><i class="fa fa-phone"></i><p>7(099)2364-232-23+</p></div>
            <div class="location-text"><i class="fa fa-envelope"></i><p>example@hotmail.cpm</p></div>
          </div>
          <div class="col-md-4 col-sm-12 ">
            <div class="location-text"><i class="fa fa-map-marker"></i><p>119333, Bld 1, 55/1, Leninsky Prospekt, Moscow, Russian Federation</p></div>
            <div class="location-text"><i class="fa fa-phone"></i><p>7(099)2364-232-23+</p></div>
            <div class="location-text"><i class="fa fa-phone"></i><p>7(099)2364-232-23+</p></div>
            <div class="location-text"><i class="fa fa-envelope"></i><p>example@hotmail.cpm</p></div>
          </div>
         </div>
       </div>
    </div>



    