
    <!--banner-section-->
    <div id="banner-section-2">
      
        <div id="" class="" data-ride="carousel">
   
          
          <div class="">
            <div class="">
              <img class="d-block w-100" src="images/home-img-hanger-1.png" alt="First slide">
              <div class="text-slider">
              <a class="btn-menu" data-toggle="modal" data-target="#menuModal">

              </a>

              <div class="main-header">

              <div class="text-left">
              <h1 >News

              </h1>
              <h3>
                Learn about our latest news and events

              </h3>

              <div class="justify-content-center div-a">
                <a href="" class="btn-homely-primary">
                  Home
                </a>
                <span class="tag-page"><i class="fa fa-arrow-right"></i></span>
                <a class="green-color" href="" class="btn-homely-primary">
                  News
                </a>
              </div>
              </div>
              </div>
              </div>
            </div>
          
          
       
           
          </div>
        </div>
    </div>

    <!--News-->
    <div id="News-page">
   
      <div class="container">
        <div class="col-sm-12">
        <div class="row">
          <div class="serch-sec">
            <form action="">
              <input type="text" placeholder="Search"><i class="fa fa-search"></i>
            </form>
          </div></div>
          <div class="col-sm-12">
            
            <a href=""><div class="news-text">
              <div class="callery-card">
                <div class="title-text-news">
                <h4>
                  <p class="big-title"><i class="	fas fa-angle-double-right"></i>Mosaic CEO Announces Partnership with FAMU</p>
                  <p class="sub-title">FOCUS ON ONGOING EDUCATIONAL OPPORTUNITIES</p>
                  <p class="date-text">March 9/2022</p>
                </h4>
              </div>
                <img src="images/Gap-Report-l.png" alt="">

            </div>
              <p class="news-des">Mosaic CEO Joc O’Rourke, and Mosaic employees recently traveled to Florida Agricultural & Mechanical University (FAMU) to launch an ongoing partnership with the School of Business and Industry (SBI) that includes internship or cooperative education openings for students, joint research projects, and employment opportunities. To kick start the collaboration, Mosaic announced a donation to the SBI.  During the visit, Joc along with Ben
                Read More</p>
              
              </div></a>
          </div>
          <div class="col-sm-12">
            
            <a href=""><div class="news-text">
              <div class="callery-card">
                <div class="title-text-news">
                <h4>
                  <p class="big-title">Mosaic CEO Announces Partnership with FAMU</p>
                  <p class="sub-title">FOCUS ON ONGOING EDUCATIONAL OPPORTUNITIES</p>
                  <p class="date-text">March 9/2022</p>
                </h4>
              </div>
                <img src="images/Gap-Report-l.png" alt="">

            </div>
              <p class="news-des">Mosaic CEO Joc O’Rourke, and Mosaic employees recently traveled to Florida Agricultural & Mechanical University (FAMU) to launch an ongoing partnership with the School of Business and Industry (SBI) that includes internship or cooperative education openings for students, joint research projects, and employment opportunities. To kick start the collaboration, Mosaic announced a donation to the SBI.  During the visit, Joc along with Ben
                Read More</p>
              
              </div></a>
          </div>
          <div class="col-sm-12">
            
            <a href=""><div class="news-text">
              <div class="callery-card">
                <div class="title-text-news">
                <h4>
                  <p class="big-title">Mosaic CEO Announces Partnership with FAMU</p>
                  <p class="sub-title">FOCUS ON ONGOING EDUCATIONAL OPPORTUNITIES</p>
                  <p class="date-text">March 9/2022</p>
                </h4>
              </div>
                <img src="images/Gap-Report-l.png" alt="">

            </div>
              <p class="news-des">Mosaic CEO Joc O’Rourke, and Mosaic employees recently traveled to Florida Agricultural & Mechanical University (FAMU) to launch an ongoing partnership with the School of Business and Industry (SBI) that includes internship or cooperative education openings for students, joint research projects, and employment opportunities. To kick start the collaboration, Mosaic announced a donation to the SBI.  During the visit, Joc along with Ben
                Read More</p>
              
              </div></a>
          </div>
          <div class="col-sm-12">
            
            <a href=""><div class="news-text">
              <div class="callery-card">
                <div class="title-text-news">
                <h4>
                  <p class="big-title">Mosaic CEO Announces Partnership with FAMU</p>
                  <p class="sub-title">FOCUS ON ONGOING EDUCATIONAL OPPORTUNITIES</p>
                  <p class="date-text">March 9/2022</p>
                </h4>
              </div>
                <img src="images/Gap-Report-l.png" alt="">

            </div>
              <p class="news-des">Mosaic CEO Joc O’Rourke, and Mosaic employees recently traveled to Florida Agricultural & Mechanical University (FAMU) to launch an ongoing partnership with the School of Business and Industry (SBI) that includes internship or cooperative education openings for students, joint research projects, and employment opportunities. To kick start the collaboration, Mosaic announced a donation to the SBI.  During the visit, Joc along with Ben
                Read More</p>
              
              </div></a>
          </div>
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item"><a class="page-link" href="#"><i class="	fas fa-angle-right"></i></a></li>
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#"><i class="	fas fa-angle-left"></i></a></li>
            </ul>
          </nav>
         
        </div>
      </div>
    </div>

