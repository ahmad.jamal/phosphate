
    <!---->
    <div id="product-page">
   
      <div class="container">
     
        <div class="row">
          <div class=" col-sm-12"> 
          <div class="justify-content-center div-a">
            <a href="" class="btn-homely-primary">
              Home
            </a>
            <span class="tag-page"><i class="fa fa-arrow-right"></i></span>
            <a class="green-color" href="">
              Contact Us
            </a>
          </div>
          </div>
          <div class="col-md-6 col-sm-12"> 
            <div class="product-details">
              <h2>Urea <span class="sub-title-product">N 46.2  </span></h2>
              
              <p class="product-details-text">The most concentrated granular nitrogen fertilizer to provide agricultural plants with nitrogen throughout the growth and development period, supplying plants with all three forms of open-access nitrogen: amide, ammonium and nitrate (after transformation in soil). It is suitable for soils with pH < 6.5. Its transformation in soil results in alkalization and further acidification of the soil solution. This nitrogen fertilizer is the most eco-friendly and harmless for plants, providing a wide range of uses </p>
              <a href=""> Dawnload<img src="images/pdf.png" alt=""></a>
            </div>
          </div>
           <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/Group43.png" alt=""> </div>
        
    
        
         
        </div>
   
    </div>

    
    <!--descriptionproduct-->
    <div id="descriptionproduct">
      <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12"><p class="title-desc">Application</p>
          <div class="desc-text">
            <p>Period: Autumn, Spring, Summer</p><img src="images/leaf.png" alt="">
          </div>
          <div class="desc-text">
            <p>Method: Broadcasting</p><img src="images/leaf.png" alt="">
          </div>
          <div class="desc-text">
            <p>Soils: Acid soils (pH < 6.5)</p><img src="images/leaf.png" alt="">
          </div>
         
        </div>
        <div class="col-md-6 col-sm-12"><p class="title-desc">Advantages</p>
          <div class="desc-text">
            <p>Provides highly effective nitrogen nutrition with a prolonged effect</p><img src="images/leaf.png" alt="">
          </div>
          <div class="desc-text">
            <p>Provides highly effective nitrogen nutrition with a prolonged effect</p><img src="images/leaf.png" alt="">
          </div>
          <div class="desc-text">
            <p>Provides highly effective nitrogen nutrition with a prolonged effect</p><img src="images/leaf.png" alt="">
          </div>
          <div class="desc-text">
            <p>Provides highly effective nitrogen nutrition with a prolonged effect</p><img src="images/leaf.png" alt="">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          </div>
        <div class="col-md-6 col-sm-12"><p class="title-desc">Specifications</p>
        <img class="img-details" src="images/home-img-hanger-1.png" alt="">
        </div>
   
      </div>
      </div>
    </div>

       <!--group-product-->
       <div id="group-product">
        <div class="container">
           <div class="right-title"><p>Other</p></div>
        
            <div id="owl-carousel3" class="owl-carousel owl-theme">
              <div class="item">
                <div class="row">
                  <div class="col-md-6 col-sm-12"> 
                    <div class="product-details">
                      <h2>Urea</h2>
                      <p class="sub-title-product">N 46.2   </p>
                      <p>The most concentrated granular nitrogen fertilizer  </p>
                      
                    </div>
                  </div>
                   <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                  
                 </div>
              </div>
              
              <div class="item">
                <div class="row">
                  <div class="col-md-6 col-sm-12"> 
                    <div class="product-details">
                      <h2>Urea</h2>
                      <p class="sub-title-product">N 46.2   </p>
                      <p>The most concentrated granular nitrogen fertilizer  </p>
                     
                    </div>
                  </div>
                   <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                  
                 </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-md-6 col-sm-12"> 
                    <div class="product-details">
                      <h2>Urea</h2>
                      <p class="sub-title-product">N 46.2   </p>
                      <p>The most concentrated granular nitrogen fertilizer  </p>
                     
                    </div>
                  </div>
                   <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                  
                 </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-md-6 col-sm-12"> 
                    <div class="product-details">
                      <h2>Urea</h2>
                      <p class="sub-title-product">N 46.2   </p>
                      <p>The most concentrated granular nitrogen fertilizer  </p>
                      
                    </div>
                  </div>
                   <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                  
                 </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-md-6 col-sm-12"> 
                    <div class="product-details">
                      <h2>Urea</h2>
                      <p class="sub-title-product">N 46.2   </p>
                      <p>The most concentrated granular nitrogen fertilizer  </p>
                  
                    </div>
                  </div>
                   <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/product.png" alt=""> </div>
                  
                 </div>
              </div>
              
           
          </div>
        </div>
      </div>

