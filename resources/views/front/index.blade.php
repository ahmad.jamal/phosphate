
    <!--banner-section-->
    <div id="banner-section">
      
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"><p class="slider-num">Start</p></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"><p class="slider-num">01</p></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"><p class="slider-num">02</p></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"><p class="slider-num">03</p></li>
            
          </ol>
          
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="images/ag-major-role-phosphorus-img1b.png" alt="First slide">
              <div class="text-slider">
              <a class="btn-menu" data-toggle="modal" data-target="#menuModal">

              </a>

              <div class="main-header">

              <div class="text-left">
              <h1 >High Phosphorus Foods for a Plant

              </h1>
              <h3>
              Phosphate and phosphorous are vital for life and growth. Discover how this rock is formed, what it's used for, and why it is so important in feeding a growing world.

              </h3>

              <div class="justify-content-center div-a">
                <a href="https://homelydesign.ca/system/public/website/get_started" class="btn-homely-primary">
                  All Product
                </a>
              </div>
              </div>
              </div>
              </div>
            </div>
            <div class="carousel-item ">
              <img class="d-block w-100" src="images/Performance-l.jpeg" alt="First slide">
              <div class="text-slider">
              <a class="btn-menu" data-toggle="modal" data-target="#menuModal">

              </a>

              <div class="main-header">

              <div class="text-left">
              <h1 >High Phosphorus Foods for a Plant

              </h1>
              <h3>
              Phosphate and phosphorous are vital for life and growth. Discover how this rock is formed, what it's used for, and why it is so important in feeding a growing world.

              </h3>

              <div class="justify-content-center div-a">
                <a href="" class="btn-homely-primary">
                  All Product
                </a>
              </div>
              </div>
              </div>
              </div>
            </div>
            <div class="carousel-item ">
              <img class="d-block w-100" src="images/Underground-1024x668.jpg" alt="First slide">
              <div class="text-slider">
              <a class="btn-menu" data-toggle="modal" data-target="#menuModal">

              </a>

              <div class="main-header">

              <div class="text-left">
              <h1 >High Phosphorus Foods for a Plant

              </h1>
              <h3>
              Phosphate and phosphorous are vital for life and growth. Discover how this rock is formed, what it's used for, and why it is so important in feeding a growing world.

              </h3>

              <div class="justify-content-center div-a">
                <a href="" class="btn-homely-primary">
                  All Product
                </a>
              </div>
              </div>
              </div>
              </div>
            </div>
            <div class="carousel-item ">
              <img class="d-block w-100" src="images/ag-major-role-phosphorus-img1b.png" alt="First slide">
              <div class="text-slider">
              <a class="btn-menu" data-toggle="modal" data-target="#menuModal">

              </a>

              <div class="main-header">

              <div class="text-left">
              <h1 >High Phosphorus Foods for a Plant

              </h1>
              <h3>
              Phosphate and phosphorous are vital for life and growth. Discover how this rock is formed, what it's used for, and why it is so important in feeding a growing world.

              </h3>

              <div class="justify-content-center div-a">
                <a href="" class="btn-homely-primary">
                  All Product
                </a>
              </div>
              </div>
              </div>
              </div>
            </div>
           
          </div>
        </div>
    </div>
    <!--banner-end-->
    <!--News-->
    <div id="news">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <div class="news-text">
            <p>Moody’s, Fitch and 
              S&amp;P Withdraw Credit 
              Ratings from Russian 
              Companies, Including 
              PhosAgro</p>
              <div  class="footer-all-news">
                <a href=""> All News</a>
                <p>10 March</p>
               
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="news-text">
            <p>Moody’s, Fitch and 
              S&amp;P Withdraw Credit 
              Ratings from Russian 
              Companies, Including 
              PhosAgro</p>
              <div class="footer-all-news">
                <a href=""> All News</a>
                <p>10 March</p>
              
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="news-text">
            <p>Moody’s, Fitch and 
              S&amp;P Withdraw Credit 
              Ratings from Russian 
              Companies, Including 
              PhosAgro</p>
              <div  class="footer-all-news">
                <a href=""> All News</a>
                <p>10 March</p>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 
    <!--product-->
    <div id="product">
      <div class="container">
        <div class="row">
      
          <div class="owl-carousel owl-theme">
            <div class="item">
             <div class="row">
              <div class="col-md-6 col-sm-12"> 
                <div class="product-details">
                  <h2>Urea</h2>
                  <p class="sub-title-product">N 46.2   </p>
                  <p>The most concentrated granular nitrogen fertilizer  </p>
                  <a href="">All Product</a>
                </div>
              </div>
               <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/Group43.png" alt=""> </div>
              
             </div>
            </div> 
            <div class="item">
              <div class="row">
               <div class="col-md-6 col-sm-12"> 
                 <div class="product-details">
                   <h2>Urea</h2>
                   <p class="sub-title-product">N 46.2   </p>
                   <p>The most concentrated granular nitrogen fertilizer  </p>
                   <a href="">All Product</a>
                 </div>
               </div>
                <div class="col-md-6 col-sm-12 right-sec-img"><img class="product-img" src="images/Group43.png" alt=""> </div>
               
              </div>
             </div> 
          </div>
        </div>
      </div>
    </div>

    <!--about-us-->
    <div id="about-us">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12"> <img src="images/MaskGroup2.png" alt=""></div>
          <div class="col-md-6 col-sm-12">
            <div class="left-sec-text">
              <h3>?What is phosphate used for</h3>
              <P>Phosphate rock is processed to produce phosphorous, which is one of the three main nutrients most commonly used in fertilizers (the other two are nitrogen and potassium). Phosphate can also be turned into phosphoric acid, which is used in everything from food and cosmetics to animal feed and electronics. At OCP we adapt our phosphate resources to deliver customized fertilizers for the </P>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!--video-->
    <div id="video">
    <div>
      <video width="320" height="240" loop autoplay>
        <source src="images/20220416_214230.mp4" type="video/mp4">
      
      </video>
    </div>
    </div>

