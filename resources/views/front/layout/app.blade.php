<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Phosphat</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="keywords" />
    <meta content="" name="description" />

    <!-- Favicons -->
    <link href="images/xbcgxfghfxfyf.ico" rel="icon" />

    <!-- Google Fonts -->
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
      rel="stylesheet"
    />

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Libraries CSS Files -->
    <link href="lib/font-awesome/css/all.min.css" rel="stylesheet" />
    <link href="lib/font-awesome/css/fontawesome.min.css" rel="stylesheet" />
    <link href="lib/animate/animate.min.css" rel="stylesheet" />

    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/top-menu.css" rel="stylesheet"/>
    <link href="css/our-mision.css" rel="stylesheet" />
    <link href="css/logistic.css" rel="stylesheet" />
 
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.theme.default.css" />
   <style>
     #product .owl-stage-outer, #product .owl-stage-outer {
    direction: ltr;
}
   </style>
  <body>
    <!--header-->
    <header id="header" class="fixed-top">
      <div class="logo">
        <a href="index.html"
          >Logo</a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul class="menu-list">
          <li class="activeMenuTab">
            <a href="index.html">
              <p class="menu-p">Home</p>
            </a>
          </li>

          <li>
            <a href="#">
              <p class="menu-p">Product</p>
            </a>
          </li>
          <li>
            <a href="#">
              <p class="menu-p">About Us </p>
            </a>
          </li>
          <li>
            <a href="#">
              <p class="menu-p"> News</p>
            </a>
          </li>

          <li>
            <a href="#">
              <p class="menu-p"> Logistic Service</p>
            </a>
          </li>
        </ul>
      </nav>
      <div class="left-top-sec">
       
        <a class="top-menw-left" href="">Lg</a>
        <a class="top-menw-btn" href="">Contact Us</a>
      
      </div>
      <!-- .main-nav -->
    </header>
    <!--header end-->

    @yield('content')

    <footer>
      <div class="container">
        <div class="row text-right f-rev">
          <div class="col-lg-6 flex-img-footer">
            <div class="d-flex-footer-logo">
              <div class="footer-img">
                Logo
              </div>

            </div>
            <div class="social-links">
              <a
                href="#"
                class="facebook fadeInUp"
                data-wow-duration="1.4s"
                target="_blank"
                ><i class="fab fa-instagram"></i
              ></a>

              <!-- <a href="#" class="twitter fadeInUp" data-wow-duration="1.6s" target="_blank"><i
    class="fab fa-twitter"></i></a> -->

              <a
                href="#"
                class="facebook fadeInUp"
                data-wow-duration="1.8s"
                target="_blank"
                ><i class="fab fa-facebook-f"></i
              ></a>

              <a
                href="#"
                class="facebook fadeInUp"
                data-wow-duration="1.8s"
                target="_blank"
                ><i class="fab fa-pinterest"></i
              ></a>

              <!-- <a href="#" class="facebook  fadeInUp" data-wow-duration="2s" target="_blank"><i
    class="fab fa-linkedin-in"></i></a> -->

              <a
                href="#"
                class="linkedin fadeInUp"
                data-wow-duration="2.2s"
                target="_blank"
                ><i class="fab fa-youtube"></i
              ></a>
            </div>
            
          </div>

          <div class="col-lg-3 text-right">
            <h5 class="text-black">Pages</h5>
          
            <a href="#" class="footer-link mb-3"> <p>Home </p> </a>
            <a href="#" class="footer-link mb-3"> <p>Product </p> </a>
            <a href="#" class="footer-link mb-3"> <p>News </p> </a>
            <a href="#" class="footer-link mb-3"> <p>About Us </p> </a>
          </div>
          <div class="col-lg-3 text-right">
            <h5 class="text-black"> Contuct Us</h5>
            <a
              data-toggle="modal"
              data-target="#messageSend"
              class="footer-link mb-3"
            >
              <p>+ 7 (495) 232-96-89
                info@phosagro.ru</p>
            </a>
           
         
          </div>
        

       
        </div>
      </div>
      
    </footer>
    <div class="sub-footer">
      <div class="container">
        <div class="row text-right f-rev">
          <div class="col-lg-6 flex-img-footer">
            <div class="d-flex-footer-logo">
           <p>© PhosAgro Group of Companies 2001 — 2022</p>
            </div>
          </div>
         
          <div class="col-lg-6 text-right">
           
           <p>Made by    oso
          </p>
         
          </div>
         
        </div>
      
      </div>
    </div>


    <!-- Button trigger modal -->

    <!-- Modal -->
    <div
      class="modal fade"
      id="messageSend"
      tabindex="-1"
      role="dialog"
      aria-labelledby="messageSendTitle"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body text-center">
            <h5 class="modal-title text-orange mb-3" id="messageSendTitle">
              اتصل بنا
            </h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <i class="fas fa-arrow-right"></i>
            </button>
            <form>
              <div class="p-rel">
                <input
                  type="email"
                  class="form-control mb-3 text-center"
                  id="emailPedmed"
                  value="KHculture@mail.com"
                  readonly="readonly"
                />
                <i class="fas fa-at"></i>
              </div>
              <textarea
                class="form-control mb-3"
                id="messageBody"
                rows="7"
                dir="rtl"
                placeholder="قم بمراسلتنا فنحن دائما نرحب باقتراحاتكم"
              ></textarea>
              <button type="submit" class="btn btn-orange w-50">إرسال</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- JavaScript Libraries -->
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/jquery/jquery-migrate.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/mobile-nav/mobile-nav.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <!-- Template Main Javascript File -->
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.animate.js"></script>
    <script src="js/owl.autoplay.js"></script>
    <script src="js/owl.navigation.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
    <script>
var owl2 = $("#owl-carousel2");
  owl2.owlCarousel({
  
    margin: 10,
    loop: true,
    nav: true,
    dots:false,
    navText: [
    '<span><i class="fa fa-arrow-right"></i></span>','<span><i class="fa fa-arrow-left"></i></span>',
         
        ],

        responsive: {
                    0: {
                        items: 1,
                        nav: true,
                    },
                    768: {
                        items: 1,
                        nav: true,
                    },
                    992: {
                      items: 3,
                      stagePadding: 150,
                    },
                },

  });

    </script>

 

 

 

  </body>
</html>