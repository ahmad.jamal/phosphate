
    <!--News-->
    <div id="artical">
   
      <div class="container">
        <div class="col-sm-12">
        <div class="row">
       
          <div class="col-sm-12">
            
            <div class="news-text">
            <img class="img-news" src="images/Gap-Report-l.png" alt="">
            <p class="big-title">Mosaic CEO Announces Partnership with FAMU</p>
            <p class="sub-title">FOCUS ON ONGOING EDUCATIONAL OPPORTUNITIES</p>
            <p class="date-text">March 9/2022</p>
            <p class="news-des">A cornerstone of Mosaic’s strategic priorities is to improve aspects of the business we can control for organizational growth, and Mosaic’s shared services teams highlight the power and effectiveness of collaborating to create global synergies, share learnings, and set goals to measure organizational success. 

The Brazil Shared Solutions Center and North America Shared Services have joined forces to establish a centralized phone system for a simple way to contact anyone on the team. The South America team has benefited from North America’s implementation of Robotic Process Automation (RPA) in SAP, and the North America team is pursuing a bot software used in South America that crawls platforms like LinkedIn to help in recruiting with hard to fill positions.  

“Both teams are made up of highly-skilled and engaged employees who continuously look for innovative ways to save time and money for their colleagues and internal customers,” explained Karen Swager, SVP- Supply Chain. “While this automation does eliminate redundancies in processes and can sometimes reduce the headcount needed to perform tasks, both of these operations are growing. Identifying, creating and maintaining efficiencies requires a human touch, and the continued focus on Mosaic’s strategic priorities means the teams do a lot of work behind the scenes to make sure they’re contributing to our global progress.”  

Blazing the Trail – 2022 and Beyond 
As we move into 2022, Mosaic is focusing on accelerating technology and digitization to drive functional collaboration and efficiency across the global organization. Over this past year, the North America team has made progress in enabling employees to efficiently request help and resolve issues. They’ve written more than 400 articles, called Knowledge Articles, that share tips on how to accomplish a task and outlines how to get help, implemented new policies to modernize employee spending and reporting, have also been integral in keeping our personal protective equipment (PPE) vending machines filled, and have been cataloguing resources for K3—our newest potash mine in Saskatchewan, Canada. While this new team has a long list of projects they plan to tackle, they’re starting with the places they can make the fastest and largest impacts.  

The South America team has been expanding their reach to serve the business in new ways. In all they do, anticipating colleagues needs remains their top priority. So they’ve been identifying opportunities to make tasks simpler for the business before they even ask. Their relationship model enables them to identify pain points and areas where they can help.  

Nestle, who is globally revered as a leader in the shared services space, has modeled similar ways of partnering with internal and external customers, so the team seeks some of their inspiration from what they’ve seen work well at Nestle. One example of this is an Innovation Hub that was established in March 2021. Through the Innovation Hub, the team uses market benchmarks to anticipate both emerging technology and business needs, and they engage with third-party start-ups to learn about innovative ways to resolve issues.  

They’re also working on further integration of SAP, machine learning and improvements to an existing Chatbot self-service tool.  

With more than 400,000 internal requests completed in 2021, measuring success for these teams includes minimizing the need for requests at all, making sure shared services are effective support services, and that they continue to foster innovation with automation, analytics, continuous improvement, and enhanced user experiences. As a global organization that supports multifaceted business operations, a global yet agile approach to shared services, strengthens our core to enable the whole company to do what we do best – help the world grow the food it needs.</p>
            
            </div>
          </div>
        
    
        
         
        </div>
      </div>
    </div>

    
    <!--news-->
    <div id="group-news">
      <div class="container">
         <div class="right-title"><p>Read This Next</p></div>
      
          <div id="owl-carousel2" class="owl-carousel owl-theme">
            <div class="item">
              <div class="card" >
                <img class="card-img-top" src="images/MaskGroup2.png" alt="Card image cap">
                <div class="card-body">
                  <p class="date-card">3/2/2022</p>
                  <h5 class="card-title">Moody’s, Fitch and S&amp;P Withdraw Credit 
                  </h5>
                  <p class="card-text">from Russian Companies, Including PhosAgro
                  </p>
                  <a href="#" class="btn ">Read More</a>
                </div>
              </div>
            </div>
            
            <div class="item">
              <div class="card" >
                <img class="card-img-top" src="images/MaskGroup2.png" alt="Card image cap">
                <div class="card-body">
                  <p class="date-card">3/2/2022</p>
                  <h5 class="card-title">Moody’s, Fitch and S&amp;P Withdraw Credit 
                  </h5>
                  <p class="card-text">from Russian Companies, Including PhosAgro
                  </p>
                  <a href="#" class="btn ">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card" >
                <img class="card-img-top" src="images/MaskGroup2.png" alt="Card image cap">
                <div class="card-body">
                  <p class="date-card">3/2/2022</p>
                  <h5 class="card-title">Moody’s, Fitch and S&amp;P Withdraw Credit 
                  </h5>
                  <p class="card-text">from Russian Companies, Including PhosAgro
                  </p>
                  <a href="#" class="btn ">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card" >
                <img class="card-img-top" src="images/MaskGroup2.png" alt="Card image cap">
                <div class="card-body">
                  <p class="date-card">3/2/2022</p>
                  <h5 class="card-title">Moody’s, Fitch and S&amp;P Withdraw Credit 
                  </h5>
                  <p class="card-text">from Russian Companies, Including PhosAgro
                  </p>
                  <a href="#" class="btn ">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card" >
                <img class="card-img-top" src="images/MaskGroup2.png" alt="Card image cap">
                <div class="card-body">
                  <p class="date-card">3/2/2022</p>
                  <h5 class="card-title">Moody’s, Fitch and S&amp;P Withdraw Credit 
                  </h5>
                  <p class="card-text">from Russian Companies, Including PhosAgro
                  </p>
                  <a href="#" class="btn ">Read More</a>
                </div>
              </div>
            </div>
            
         
        </div>
      </div>
    </div>




   