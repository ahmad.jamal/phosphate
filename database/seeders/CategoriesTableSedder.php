<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Directory;
use Illuminate\Support\Facades\DB;

class CategoriesTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('categories')->insert([
                'nameAR' => $faker->name(),
                'nameEN' => $faker->name(),
                'category_id' => $faker->numberBetween($min = 0, $max = 100),
                'descriptionAR' => $faker->paragraph($nb = 8),
                'descriptionEN' => $faker->paragraph($nb = 8)
            ]);
        }
    }
}
