<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProductOptionTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 200) as $index) {
            DB::table('product_options')->insert([
                'nameAR' => $faker->name(),
                'nameEN' => $faker->name(),
                'contentAR' => $faker->paragraph($nb = 2),
                'product_id' => $faker->numberBetween($min = 420, $max = 620),
                'contentEN' => $faker->paragraph($nb = 2)
            ]);
        }
    }
}
